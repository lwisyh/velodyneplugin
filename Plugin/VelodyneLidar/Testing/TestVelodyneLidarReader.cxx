// Copyright 2013 Velodyne Acoustics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TestHelpers.h"
#include "vtkLidarReader.h"
#include "vtkVelodyneMetaPacketInterpreter.h"

#include <vtkNew.h>
#include <vtkPVXMLParser.h>
#include <vtkPVXMLElement.h>

#include <string>

using namespace std;

/**
 * @brief TestFile Runs all the tests on a given pcap and its corresponding VTP files
 * @param pcapFileName The pcap file
 * @param referenceFileName The meta-file containing the list of VTP files (baseline) to test against each frames
 * @param correctionFileName The XML sensor calibration file
 * @return 0 on success, 1 on failure
 */
int main(int argc, char* argv[])
{
  std::cout << "argc: " << argc << std::endl;
  if (argc < 5)
  {
    std::cerr << "Wrong number of arguments. Usage: TestLidarReader <pcapFileName> <referenceFileName> <correctionFileName> <reference-data.xml> <SpeedReference(Opt)>" << std::endl;

    return 1;
  }

  string pcapFileName = argv[1];
  string baseline = argv[2];
  string calibrationFileName = argv[3];
  string referenceDataXML = argv[4];

  double FrequencyReference_Hz = 10;
  if(argc > 5)
  {
    FrequencyReference_Hz = stod(argv[5]);
  }

  std::cout << "-------------------------------------------------------------------------" << std::endl
          << "Pcap :\t" << pcapFileName << std::endl
          << "Baseline:\t" << baseline << std::endl
          << "Calibration :\t" << calibrationFileName << std::endl
          << "-------------------------------------------------------------------------" << std::endl;

  // return value indicate if the test passed
  int retVal = 0;

  // Generate a Velodyne HDL reader
  vtkNew<vtkLidarReader> reader;
  vtkNew<vtkVelodyneMetaPacketInterpreter> interp;
  interp->SetIgnoreEmptyFrames(true);
  interp->SetIgnoreZeroDistances(true);
  interp->SetEnableAdvancedArrays(true);
  reader->SetInterpreter(interp);
  reader->SetShowFirstAndLastFrame(true);
  reader->SetFileName(pcapFileName);
  reader->SetCalibrationFileName(calibrationFileName);

  vtkNew<vtkPVXMLParser> parser;
  parser->SetFileName(referenceDataXML.c_str());
  if (parser->Parse() == 0 || parser->GetRootElement() == nullptr)
  {
    std::cerr << "Invalid XML in file: " << referenceDataXML << "." << std::endl;
    return 1;
  }
  vtkPVXMLElement* root = parser->GetRootElement();

  // read referenceNetworkTimeToDataTime
  const char* foundReferenceNetworkTimeToDataTime = root->GetAttribute("referenceNetworkTimeToDataTime");
  if (foundReferenceNetworkTimeToDataTime == 0)
  {
    std::cerr << "No referenceNetworkTimeToDataTime element was found." << std::endl;
    return 1;
  }

  double referenceNetworkTimeToDataTime = std::stod(foundReferenceNetworkTimeToDataTime);
  retVal += testLidarReader(reader.Get(),referenceNetworkTimeToDataTime, baseline, FrequencyReference_Hz);

  return  retVal;
}
