
// Copyright 2013 Velodyne Acoustics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TestHelpers.h"
#include "vtkLidarStream.h"
#include "vvPacketSender.h"
#include "vtkVelodyneMetaPacketInterpreter.h"

#include <vtkNew.h>
#include <vtkTimerLog.h>

#include <boost/thread/thread.hpp>

/**
 * @brief TestFile Runs all the tests on a given pcap and its corresponding VTP files
 * @param correctionFileName The corrections to use
 * @param pcapFileName Input PCAP file
 * @param vtpFileName meta-file containing the list of files to test against each frames
 * @return 0 on success, 1 on failure
 */
int main(int argc, char* argv[])
{
  if (argc < 4)
  {
    std::cerr << "Wrong number of arguments. Usage: TestLidarStream <pcapFileName> <referenceFileName> <correctionFileName>" << std::endl;
    return 1;
  }

  // return value indicate if the test passed
  int retVal = 0;

  // get command line parameter
  std::string pcapFileName = argv[1];
  std::string referenceFileName = argv[2];
  std::string correctionFileName = argv[3];

  std::cout << "-------------------------------------------------------------------------" << std::endl
            << "Pcap :\t" << pcapFileName << std::endl
            << "Baseline:\t" << referenceFileName << std::endl
            << "Corrections :\t" << correctionFileName << std::endl
            << "-------------------------------------------------------------------------" << std::endl;

  const int dataPort = 2368;
  const bool isLiveCalibration = (correctionFileName == "");
  const bool shouldPreSend = isLiveCalibration;

  //
  vtkSmartPointer<vtkLidarStream> LidarStream = vtkSmartPointer<vtkLidarStream>::New();
  auto interp = vtkSmartPointer<vtkVelodyneMetaPacketInterpreter>::New();
  interp->SetIgnoreEmptyFrames(true);
  interp->SetIgnoreZeroDistances(true);
  interp->SetEnableAdvancedArrays(true);
  LidarStream->SetInterpreter(interp);
  LidarStream->SetCalibrationFileName(correctionFileName);
  LidarStream->SetListeningPort(dataPort);
  LidarStream->SetIsCrashAnalysing(false);
  LidarStream->SetIsForwarding(false);
  retVal += testLidarStream(LidarStream.Get(), shouldPreSend, 1e3, 1e3, pcapFileName, referenceFileName);

  return retVal;
}


