#include "vtkVelodyneHDLPositionPacketInterpreter.h"

#pragma pack(push, 1)
struct PositionPacket
{
  // tohTimestamp is a microseconds rolling counter provided by the lidar
  // internal clock which will be adjusted using the gps data (if one is plugged)
  // tohTimestamp gives the number of microseconds ellapsed since the top of
  // the hour. The top of the hour is defined as (modulo 1 hour):
  // - without a gps plugged: the instant when the lidar was powered on
  // - with a gps: any full UTC hour (such as 12:00:00 am, UTC)
  unsigned int tohTimestamp;
  short gyro[3];
  short temp[3];
  short accelx[3];
  short accely[3];
  // Usage of field PPS (Pulse Per Second signal) is explained in PDF document
  // "Webserver User Guide (VLP-16 & HDL-32E)" available at
  // https://velodynelidar.com/downloads.html#application_notes
  unsigned char PPSSync;
  // UDP position packet is 554 bytes long with 42 bytes of header and 512 bytes
  // of payload. In the payload, 512 - (198 + 4 + 1 + 3) = 306 are available for
  // the NMEA sequence.
  // The last bytes stored in the sentence char array should be 0 (NMEA
  // sentences are not that long).
  char sentance[306];
};
#pragma pack(pop)

const unsigned short BIT_12_MASK = 0x0fff;
const unsigned short REMAINDER_12_MASK = 0x07ff;
const unsigned short SIGN_12_MASK = 0x0800;

const double GYRO_SCALE = 0.09766;   // deg / s
const double TEMP_SCALE = 0.1453;    // C
const double TEMP_OFFSET = 25.0;     // C
const double ACCEL_SCALE = 0.001221; // G
