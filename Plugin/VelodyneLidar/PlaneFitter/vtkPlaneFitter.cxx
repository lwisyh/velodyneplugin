// Copyright 2013 Velodyne Acoustics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "vtkPlaneFitter.h"
#include "vtkHelper.h"
#include <Eigen/Dense>

#include <vtkObjectFactory.h>
#include <vtkPointSet.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkVariantArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkStringArray.h>

#include <vtkMath.h>
#include <vtkTable.h>
#include <vtkThreshold.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkPlaneSource.h>

namespace {
  //-----------------------------------------------------------------------------
  struct PlaneStat {
    std::string channelId = "";
    Eigen::Vector3d origin = Eigen::Vector3d::Zero();
    Eigen::Vector3d normal = Eigen::Vector3d::Zero();
    double semiDist = 0.;
    double nPts = 0.;
    double mean = 0.;
    double stddev = 0.;
    double rms = 0.;
  };

  // if updateNormal is true, all stats are computed from rawData
  // else they are all calculated except semiDist, origin and normal
  bool ComputeStat(vtkSmartPointer<vtkDoubleArray> rawData,
                   std::string const & channelId,
                   PlaneStat& stat,
                   bool const & updateNormal = true)
  {
    if (!rawData)
    {
      vtkGenericWarningMacro("rawData empty");
      return false;
    }

    unsigned int const nTuples = rawData->GetNumberOfTuples();
    Eigen::Map<Eigen::MatrixXd> matData(static_cast<double*>(rawData->GetVoidPointer(0)),
        rawData->GetNumberOfComponents(), nTuples);

    double semiDist = 0.;
    Eigen::Vector3d origin = Eigen::Vector3d::Zero();
    Eigen::Vector3d normal = Eigen::Vector3d::Zero();
    Eigen::MatrixXd centeredData;

    if (updateNormal)
    {
      if (nTuples <= 2)
      {
        vtkGenericWarningMacro("Not enough tuples to compute normal");
        return false;
      }

      origin = matData.rowwise().mean();
      centeredData = (matData.colwise() - origin).transpose();
      Eigen::JacobiSVD<Eigen::MatrixXd> svd(centeredData, Eigen::ComputeThinU | Eigen::ComputeThinV);
      Eigen::VectorXd D = svd.singularValues();
      normal = svd.matrixV().col(2);
      semiDist = std::sqrt(D(0));

      // Update computed semiDist, origin and normal
      stat.semiDist = semiDist;
      stat.origin = origin;
      stat.normal = normal;
    }
    else
    {
      // Get semiDist, origin and normal from stat
      semiDist = stat.semiDist;
      origin = stat.origin;
      normal = stat.normal;

      centeredData = (matData.colwise() - origin).transpose();
    }

    // check if normal is valid
    if ( semiDist < std::numeric_limits<double>::min() || normal.isZero(0) || (std::fabs(normal.norm() - 1.0) > 1.0e-8) )
    {
      vtkGenericWarningMacro("Invalid plane parameters");
      return false;
    }

    Eigen::VectorXd distanceData = centeredData * normal;

    if (distanceData.size() != nTuples)
    {
      vtkGenericWarningMacro("Inconsistent number of points in plane distances");
      return false;
    }

    // Update remaining stats
    stat.channelId = channelId;
    stat.nPts = nTuples;
    stat.mean = distanceData.mean();
    stat.stddev = std::sqrt((distanceData.array() - stat.mean).matrix().squaredNorm() / (stat.nPts-1));
    stat.rms = std::sqrt(std::pow(stat.mean,2.) + std::pow(stat.stddev,2.));

    return true;
  }

  // Convert plane stats to vtkTable
  vtkSmartPointer<vtkTable> convertToTable(std::vector<PlaneStat>& vStats)
  {
    unsigned int size = vStats.size();
    unsigned int index = 0;

    // Init Arrays for Table output
    auto channelCol = createArray<vtkStringArray>("channel", 1, size);
    auto originXCol = createArray<vtkDoubleArray>("originx", 1, size);
    auto originYCol = createArray<vtkDoubleArray>("originy", 1, size);
    auto originZCol = createArray<vtkDoubleArray>("originz", 1, size);
    auto normalXCol = createArray<vtkDoubleArray>("normalx", 1, size);
    auto normalYCol = createArray<vtkDoubleArray>("normaly", 1, size);
    auto normalZCol = createArray<vtkDoubleArray>("normalz", 1, size);
    auto meanCol    = createArray<vtkDoubleArray>("mean"   , 1, size);
    auto stdDevCol  = createArray<vtkDoubleArray>("stddev" , 1, size);
    auto rmsCol     = createArray<vtkDoubleArray>("rms"    , 1, size);
    auto ptsNbCol   = createArray<vtkDoubleArray>("npts"   , 1, size);

    // Add row informations
    for(auto stats : vStats)
    {
      channelCol->SetValue(index, stats.channelId);
      originXCol->SetValue(index, stats.origin[0]);
      originYCol->SetValue(index, stats.origin[1]);
      originZCol->SetValue(index, stats.origin[2]);
      normalXCol->SetValue(index, stats.normal[0]);
      normalYCol->SetValue(index, stats.normal[1]);
      normalZCol->SetValue(index, stats.normal[2]);
      ptsNbCol->SetValue(index, stats.nPts);
      meanCol->SetValue(index, stats.mean);
      stdDevCol->SetValue(index, stats.stddev);
      rmsCol->SetValue(index, stats.rms);

      index++;
    }

    // Add columns to vtkTable
    vtkSmartPointer<vtkTable> table = vtkSmartPointer<vtkTable>::New();
    table->AddColumn(channelCol);
    table->AddColumn(originXCol);
    table->AddColumn(originYCol);
    table->AddColumn(originZCol);
    table->AddColumn(normalXCol);
    table->AddColumn(normalYCol);
    table->AddColumn(normalZCol);
    table->AddColumn(meanCol);
    table->AddColumn(stdDevCol);
    table->AddColumn(rmsCol);
    table->AddColumn(ptsNbCol);

    return table;
  }

  // Create a vtk plane source
  vtkSmartPointer<vtkPolyData> CreatePlane(double semiDist, Eigen::Vector3d origin, Eigen::Vector3d normal)
  {
    vtkSmartPointer<vtkPlaneSource> plane = vtkSmartPointer<vtkPlaneSource>::New();
    plane->SetOrigin(-semiDist, -semiDist, 0.0);
    plane->SetPoint1(semiDist, -semiDist, 0.0);
    plane->SetPoint2(-semiDist, semiDist, 0.0);
    plane->SetResolution(semiDist*2, semiDist*2);
    plane->SetCenter(origin[0], origin[1], origin[2]);
    plane->SetNormal(normal[0], normal[1], normal[2]);
    plane->Update();

    return plane->GetOutput();
  }
}

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkPlaneFitter)

//-----------------------------------------------------------------------------
vtkPlaneFitter::vtkPlaneFitter()
{
  this->SetNumberOfInputPorts(1);
  this->SetNumberOfOutputPorts(2);
}

//-----------------------------------------------------------------------------
vtkPlaneFitter::~vtkPlaneFitter()
{
}

//-----------------------------------------------------------------------------
void vtkPlaneFitter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
int vtkPlaneFitter::FillInputPortInformation(int port, vtkInformation *info)
{
  if (port == 0)
  {
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPointSet");
    return 1;
  }

  return 0;
}

//----------------------------------------------------------------------------
int vtkPlaneFitter::FillOutputPortInformation(int port, vtkInformation *info)
{
  if (port == 0)
  {
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkTable");
    return 1;
  }

  if (port == 1)
  {
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPolyData");
    return 1;
  }

  return 0;
}

//-----------------------------------------------------------------------------
int vtkPlaneFitter::RequestData(vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector, vtkInformationVector *outputVector)
{
  // Get the input
  vtkPointSet* input = vtkPointSet::GetData(inputVector[0]->GetInformationObject(0));
  if (!input)
  {
    vtkErrorMacro("Null pointer entry, can not launch the filter");
    return VTK_ERROR;
  }

  vtkDataArray* laserIndexArray = this->GetInputArrayToProcess(0, inputVector);
  if (!laserIndexArray)
  {
    vtkErrorMacro( "The array " << laserIndexArray->GetName()  << " is not valid, please update laserIDArray.");
    return VTK_ERROR;
  }

  if(!input->GetPointData() || input->GetNumberOfPoints() <= 2)
  {
    vtkErrorMacro("Not enough points selected");
    return VTK_ERROR;
  }

  vtkSmartPointer<vtkDoubleArray> data = vtkSmartPointer<vtkDoubleArray>::New();
  data->DeepCopy(input->GetPoints()->GetData());
  unsigned int max_laser_id = laserIndexArray->GetRange()[1];
  unsigned int channelNb = static_cast<unsigned int>(std::pow(2, vtkMath::CeilLog2(max_laser_id)));

  if(data->GetNumberOfComponents() != 3)
  {
    vtkErrorMacro("Invalid number of components");
    return VTK_ERROR;
  }

  if (data->GetNumberOfTuples() == 0)
  {
    vtkErrorMacro("Not enough tuples");
    return VTK_ERROR;
  }

  // Compute overall stats
  std::vector<PlaneStat> vStats;
  PlaneStat overallStats, channelStats;
  if(!ComputeStat(data, std::string("overall"), overallStats))
  {
    vtkErrorMacro("Failed to compute overall statistics");
    return VTK_ERROR;
  }

  vStats.push_back(overallStats);

  // Compute per channel stats
  vtkSmartPointer<vtkThreshold> threshold = vtkSmartPointer<vtkThreshold>::New();
  threshold->SetInputData(input);
  threshold->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, laserIndexArray->GetName());
  threshold->SetOutputPointsPrecision(vtkAlgorithm::DEFAULT_PRECISION);

  // Update stats for each channel with overall stats
  channelStats.semiDist = overallStats.semiDist;
  channelStats.origin = overallStats.origin;
  channelStats.normal = overallStats.normal;

  for (unsigned int i = 0; i < channelNb; ++i)
  {
    threshold->ThresholdBetween(i, i);
    threshold->Update();
    data->DeepCopy(threshold->GetOutput()->GetPoints()->GetData());

    if (data->GetNumberOfTuples() == 0)
    {
      continue;
    }

    if(!ComputeStat(data, std::to_string(i), channelStats, false))
    {
      vtkErrorMacro("Failed to compute per channel statistics");
      return VTK_ERROR;
    }

    vStats.push_back(channelStats);
  }

  if (vStats.size() > channelNb + 1)
  {
    vtkErrorMacro("Inconsistent channel number");
    return VTK_ERROR;
  }

  // Update outputs
  // Update vtkTable output with vStats
  vtkTable* outputTable = vtkTable::GetData(outputVector->GetInformationObject(0));
  outputTable->ShallowCopy(convertToTable(vStats));

  // Set the Output plane source
  vtkPolyData* outputPlane = vtkPolyData::GetData(outputVector->GetInformationObject(1));
  outputPlane->ShallowCopy(CreatePlane(overallStats.semiDist, overallStats.origin, overallStats.normal));

  return VTK_OK;
}
