// Copyright 2013 Velodyne Acoustics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// .NAME vtkPlaneFitter - class for fitting a plane to selected PointSet
// .Section Description
//

#ifndef _vtkPlaneFitter_h
#define _vtkPlaneFitter_h

#include <vtkTableAlgorithm.h>
#include <vtkPolyDataAlgorithm.h>
#include <vtkSmartPointer.h>
#include <vtkDoubleArray.h>

#include "VelodyneLidarModule.h"

class vtkPointSet;

class VELODYNELIDAR_EXPORT vtkPlaneFitter : public vtkPolyDataAlgorithm
{
public:
  static vtkPlaneFitter* New();
  vtkTypeMacro(vtkPlaneFitter, vtkPolyDataAlgorithm)
  void PrintSelf(ostream& os, vtkIndent indent) override;

protected:
  vtkPlaneFitter();
  ~vtkPlaneFitter() override;

  int FillInputPortInformation(int port, vtkInformation *info) override;
  int FillOutputPortInformation(int port, vtkInformation *info) override;
  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *) override;

private:
  vtkPlaneFitter(const vtkPlaneFitter&);
  void operator=(const vtkPlaneFitter&);
};

#endif
