#ifndef VELODYNEMETAPACKETINTERPRETOR_H
#define VELODYNEMETAPACKETINTERPRETOR_H

#include "vtkLidarPacketInterpreter.h"

#include <vtkEvent.h>
#include <vtkNew.h>
#include <vtkCollection.h>
#include <vtkDoubleArray.h>

#include "vtkVelodyneLegacyPacketInterpreter.h"
#include "vtkVelodyneAdvancedPacketInterpreter.h"
#ifdef VELOVIEW_HAS_VELARRAY
#include "vtkVelodyneSpecialVelarrayPacketInterpreter.h"
#endif

#include "VelodyneLidarModule.h"

const unsigned long FirstSwitchOfInterpreterEvent = 60000;

#define GenericSetMacro(name,type) \
void Set##name (type _arg) override \
{ \
  if (this->SelectedInterp) \
  { \
    this->SelectedInterp->Set##name(_arg);\
  } \
  else \
  { \
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i) \
    { \
      vtkLidarPacketInterpreter* pt = vtkLidarPacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i)); \
      pt->Set##name(_arg); \
    } \
  } \
}

#define GenericGetMacro(name, type) \
type Get##name () override \
{ \
  if (this->SelectedInterp) \
  { \
    return this->SelectedInterp->Get##name();\
  } \
  vtkLidarPacketInterpreter* pt = vtkLidarPacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(0)); \
  return pt->Get##name(); \
}

#define SpecificSetMacro(name,type) \
void Set##name (type _arg) \
{ \
  if (this->SelectedInterp) \
  { \
    this->SelectedInterp->Set##name(_arg); \
  } \
  else \
  { \
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i) \
    { \
      vtkVelodyneBasePacketInterpreter::SafeDownCast(PotentialInterps->GetItemAsObject(i))->Set##name(_arg); \
    } \
  } \
}

#define SpecificGetMacro(name, type) \
type Get##name () \
{ \
  if (this->SelectedInterp) \
  { \
    return this->SelectedInterp->Get##name(); \
  } \
  return vtkVelodyneBasePacketInterpreter::SafeDownCast(PotentialInterps->GetItemAsObject(0))->Get##name(); \
}


#define SpecificGetPtrMacro(name, ptrtype) \
ptrtype Get##name () \
{ \
  if (this->SelectedInterp) \
  { \
    return this->SelectedInterp->Get##name(); \
  } \
  return vtkVelodyneBasePacketInterpreter::SafeDownCast(PotentialInterps->GetItemAsObject(0))->Get##name(); \
}

#define redirect_directly_function(function_call) \
  assert(this->SelectedInterp); \
  return this->SelectedInterp->function_call;

//!
//! \brief The vtkVelodyneMetaPacketInterpreter class is a meta interpreter that hold different Velodyne Interpeter and redict all
//! call to the right one
//!
class VELODYNELIDAR_EXPORT vtkVelodyneMetaPacketInterpreter : public vtkLidarPacketInterpreter
{
public:
  static vtkVelodyneMetaPacketInterpreter* New();
  vtkTypeMacro(vtkVelodyneMetaPacketInterpreter, vtkLidarPacketInterpreter)

  enum DualFlag
  {
    DUAL_DISTANCE_NEAR = 0x1,  // point with lesser distance
    DUAL_DISTANCE_FAR = 0x2,   // point with greater distance
    DUAL_INTENSITY_HIGH = 0x4, // point with lesser intensity
    DUAL_INTENSITY_LOW = 0x8,  // point with greater intensity
    DUAL_DOUBLED = 0xf,        // point is single return
    DUAL_DISTANCE_MASK = 0x3,
    DUAL_INTENSITY_MASK = 0xc,
  };

  void LoadCalibration(const std::string& filename) override;

  GenericGetMacro( CalibrationTable, vtkSmartPointer<vtkTable>)

  void ProcessPacket(unsigned char const * data, unsigned int dataLength) override { redirect_directly_function(ProcessPacket(data, dataLength)) }

  void ProcessPacketWrapped(unsigned char const * data, unsigned int dataLength, double PacketNetworkTime)
       override { redirect_directly_function(ProcessPacketWrapped(data, dataLength, PacketNetworkTime)) }

  bool SplitFrame(bool force = false, FramingMethod_t framingMethodAskingForSplitFrame = FramingMethod_t::INTERPRETER_FRAMING)
                  override { redirect_directly_function(SplitFrame(force, framingMethodAskingForSplitFrame)) }

  bool IsLidarPacket(unsigned char const * data, unsigned int dataLength) override;

  void ResetCurrentFrame() override;

  bool PreProcessPacket(unsigned char const * data, unsigned int dataLength,
                        fpos_t filePosition = fpos_t(), double packetNetworkTime = 0,
                        std::vector<FrameInformation>* frameCatalog = nullptr) override { redirect_directly_function(PreProcessPacket(data, dataLength, filePosition, packetNetworkTime, frameCatalog)) }

  bool PreProcessPacketWrapped(unsigned char const * data, unsigned int dataLength,
                            fpos_t filePosition = fpos_t(), double packetNetworkTime = 0,
                            std::vector<FrameInformation>* frameCatalog = nullptr)
                            override { redirect_directly_function(PreProcessPacketWrapped(data, dataLength, filePosition, packetNetworkTime, frameCatalog)) }

  bool IsNewFrameReady() override { redirect_directly_function(IsNewFrameReady()) }

  vtkSmartPointer<vtkPolyData> GetLastFrameAvailable() override { redirect_directly_function(GetLastFrameAvailable()) }

  void ClearAllFramesAvailable() override;

  std::string GetSensorInformation(bool shortVersion = false) override;

  std::string GetDefaultRecordFileName() override ;

  GenericGetMacro(ParserMetaData, FrameInformation)

  void ResetParserMetaData() override;

  void SetParserMetaData(const FrameInformation &metaData) override;

  GenericGetMacro(NumberOfChannels, int)

  GenericGetMacro(CalibrationFileName, std::string)
  GenericSetMacro(CalibrationFileName, std::string)

  GenericGetMacro(IsCalibrated, bool)

  GenericGetMacro(TimeOffset, double)
  GenericSetMacro(TimeOffset, double)

  vtkIntArray* GetLaserSelection() override;
  void SetLaserSelection(int index, int value) override;

  GenericGetMacro(DistanceResolutionM, double)

  GenericGetMacro(Frequency, double)
  GenericGetMacro(Rpm, double)

  GenericGetMacro(IgnoreZeroDistances, bool)
  GenericSetMacro(IgnoreZeroDistances, bool)

  void SetHideDropPoints(bool value)
  {
    if (this->SelectedInterp)
    {
      reinterpret_cast<vtkVelodyneBasePacketInterpreter*>(this->SelectedInterp)->SetHideDropPoints(value);
    }
    else
    {
      for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
      {
        vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
        pt->SetHideDropPoints(value);
      }
    }
  }

  GenericGetMacro(IgnoreEmptyFrames, bool)
  GenericSetMacro(IgnoreEmptyFrames, bool)

  GenericGetMacro(ApplyTransform, bool)
  GenericSetMacro(ApplyTransform, bool)

  GenericGetMacro(SensorTransform, vtkTransform*)
  GenericSetMacro(SensorTransform, vtkTransform*)

  GenericGetMacro(CropMode, int)
  GenericSetMacro(CropMode, int)

  GenericGetMacro(CropOutside, bool)
  GenericSetMacro(CropOutside, bool)

  void SetCropRegion(double _arg1, double _arg2, double _arg3, double _arg4, double _arg5, double _arg6) override;
  void SetCropRegion(double _arg[6]);

  void GetXMLColorTable(double XMLColorTable[]);

  void GetLaserCorrections(double verticalCorrection[HDL_MAX_NUM_LASERS],
    double rotationalCorrection[HDL_MAX_NUM_LASERS], double distanceCorrection[HDL_MAX_NUM_LASERS],
    double distanceCorrectionX[HDL_MAX_NUM_LASERS], double distanceCorrectionY[HDL_MAX_NUM_LASERS],
    double verticalOffsetCorrection[HDL_MAX_NUM_LASERS],
    double horizontalOffsetCorrection[HDL_MAX_NUM_LASERS], double focalDistance[HDL_MAX_NUM_LASERS],
    double focalSlope[HDL_MAX_NUM_LASERS], double minIntensity[HDL_MAX_NUM_LASERS],
                           double maxIntensity[HDL_MAX_NUM_LASERS]);

  SpecificGetMacro(HasDualReturn, bool)

  SpecificGetMacro(WantIntensityCorrection, bool)
  SpecificSetMacro(WantIntensityCorrection, bool)

  SpecificGetMacro(FiringsSkip, bool)
  SpecificSetMacro(FiringsSkip, bool)

  SpecificGetMacro(UseIntraFiringAdjustment, bool)
  SpecificSetMacro(UseIntraFiringAdjustment, bool)

  SpecificGetMacro(DualReturnFilter, bool)
  SpecificSetMacro(DualReturnFilter, bool)

  SpecificGetPtrMacro(_verticalCorrection, vtkDoubleArray*)
  SpecificGetPtrMacro(_rotationalCorrection, vtkDoubleArray*)
  SpecificGetPtrMacro(_distanceCorrection, vtkDoubleArray*)
  SpecificGetPtrMacro(_distanceCorrectionX, vtkDoubleArray*)
  SpecificGetPtrMacro(_distanceCorrectionY, vtkDoubleArray*)
  SpecificGetPtrMacro(_verticalOffsetCorrection, vtkDoubleArray*)
  SpecificGetPtrMacro(_horizontalOffsetCorrection, vtkDoubleArray*)
  SpecificGetPtrMacro(_focalDistance, vtkDoubleArray*)
  SpecificGetPtrMacro(_focalSlope, vtkDoubleArray*)
  SpecificGetPtrMacro(_minIntensity, vtkDoubleArray*)
  SpecificGetPtrMacro(_maxIntensity, vtkDoubleArray*)

  vtkMTimeType GetMTime() override;

  GenericSetMacro(EnableAdvancedArrays, bool)

  GenericSetMacro(FramingMethod, int)
  GenericGetMacro(FramingMethod, int)

  GenericSetMacro(FrameDuration_s, double)
  GenericGetMacro(FrameDuration_s, double)

protected:
    vtkVelodyneMetaPacketInterpreter();

    //! this function should never be called
    vtkSmartPointer<vtkPolyData> CreateNewEmptyFrame(vtkIdType numberOfPoints, vtkIdType prereservedNumberOfPoints = 0) override;

private:
  vtkVelodyneMetaPacketInterpreter(const vtkVelodyneMetaPacketInterpreter&) = delete;
  void operator=(const vtkVelodyneMetaPacketInterpreter&) = delete;

  //! Collection of interpreter that can be used by this meta interpreter
  vtkNew<vtkCollection> PotentialInterps;
  //! The interpreter actually used
  vtkVelodyneBasePacketInterpreter* SelectedInterp = nullptr;

  //! bool to know if a previous switch of the interpreter has been detected
  //! We need to detect the first switch to emit the signal to set a new calib file
  //! We don't need to ask the user the calibration file every time
  //! because every interpreter pt will save its own calibration file
  //! This attribute is only in the Meta interpreter class.
  bool SwitchAlreadyDetected = false;
};

#endif // VELODYNEMETAPACKETINTERPRETOR_H
