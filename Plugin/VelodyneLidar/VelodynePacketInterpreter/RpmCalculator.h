#pragma once

// Structure to compute RPM and handle degenerated cases
class RPMCalculator
{
public:
  using azimuth_t   = int;
  using timestamp_t = unsigned int; // We use Generic UInt32 Timestamps

  void ResetRPM();

  // RPM
  void SetInvert(bool state);
  double GetRPM();
  // Accepts Hundredth of Degrees Azimuth and MicroSeconds Timestamp
  void AddData(azimuth_t angle, timestamp_t rawtime);

  // Frequency
  double ComputeFrequency(timestamp_t frame_curr);
  double GetFrequency();

private:
  //RPM Calc
  // Determines if the rpm computation is available //WIP probably can put a single bool here
  enum Ready{
    NOT      = 0b0000,
    AZIM_MIN = 0b0001,
    AZIM_MAX = 0b0010,
    TIME_MIN = 0b0100,
    TIME_MAX = 0b1000,
    ALL      = 0b1111,
  };
  Ready ready;
  azimuth_t limit_azm[2];     // Azimuth   min/max
  timestamp_t limit_time[2];  // Timestamp min/max
  bool invert;
  
  friend void operator|= (Ready& a, const Ready& b);
  bool IsReady(); // Is RPM Ready

  // Frequency Calculation
  timestamp_t frame_curr;
  timestamp_t frame_last;
  timestamp_t frame_diff;
  double Frequency;
};

inline void operator|= (RPMCalculator::Ready& a, const RPMCalculator::Ready& b)
{
  a = static_cast<RPMCalculator::Ready>(static_cast<int>(a) | static_cast<int>(b));
}
